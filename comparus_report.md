#Document image classification algorithm

##Problem

There are 16000 images of documents of 16 classes (1000 images per class). In order to identify the class of some new image given, the classification algorithm should be developed.

##Solution

###Summary

To solve the problem, the neural network model has been developed. The accuracy of the model prediction is 0.579 on the test dataset and 0.61 on the validation dataset, after 20 training epochs. From the two solution approaches described below, only first was implemented, due to the lack of computational resources.

###Data preparation

The dataset was splitted randomly into three parts: training, validation and testing. The sizes of the parts obey the relation 3:1:1, respectively. All of the classes within the subsets are represented equally. The split was performed with the sklearn train_test_split function. Then the files from each of the subsets were moved to the different directories, which are 'train', 'validation' and 'test'. Test subset is neccessary to control the overfitting of the model learning hyperparameters, such as learning rate, number of units in the NN network layer, dropout rates, etc. The dataset splitting was the same during the model developement.

The images for the classification algorithm were loaded with the Keras ImageDataGenerator class. ImageDataGenerator instance is configured to divide image pixel values by 255, so rescaling images matrixes. Then, the images were loaded in grayscale, into three data iterators, train_images, val_images and test_images, from the corresponding directories. Iterators were used to optimize RAM consumption during the model training process.

All images are transformed to the same size after the loading, and the new image size is 672 height pixels and 512 width pixels. These parameters were chosen experimentally, to fit the model into the GPU memory.

Images were grouped into batches of size 32. It was found that bigger batch sizes lead to the better prediction accuracies and smoother training process. However, additional GPU memory is required for the bigger batches.

###First approach

To solve the problem, the neural network has been developed, by the means of the Tensorflow framework. It consists of the following layers:

1. Data augmentation layers, which reduce overfitting:

  * RandomZoom layer (zoom range [-0.2; 0.2])

  * RandomContrast layer (contrast range [-0.1; 0.1])

  We do not include here RandomRotation and RandomFlip layers, since most documents in the training dataset are aligned either vertically or horizontally. In other words, the rotated or flipped images do not represent the general population of documents, on which our model is going to be used. Such transformed documents will only introduce wrong samples into the training dataset. However, the given set of images contain some documents in the album orientation, which also should be treated properly. As it was found in the experiments, introduction of random rotations and flips significantly reduces model accuracy.

2. BatchNormalization layer, to increase the learning rate of the model.

3. Conv2D layer, with 8 kernels of size 5x5

4. Conv2D layer, with 8 kernels of size 3x3

5. MaxPooling2D layer, with pool_size of 2x2 and the strides of 3

6. BatchNormalization layer

7. Conv2D layer, 16 kernels of size 3x3

8. MaxPooling2D layer, with pool_size of 2x2 and strides of 3

9. BatchNormalization layer

10. Conv2D layer, 32 kernels of size 3x3

11. MaxPooling2D layer, with pool_size of 2x2 and strides of 3

12. BatchNormalization layer

13. Conv2D layer, 64 kernels of size 3x3

14. MaxPooling2D layer, with pool_size of 2x2 and strides of 3

15. BatchNormalization layer

16. Conv2D layer, 64 kernels of size 3x3

17. MaxPooling2D layer, with pool_size of 2x2 and strides of 3

18. BatchNormalization layer

19. Flatten layer, to convert the two-dimensional feature map obtained after several subsequent convolutions and poolings, to the one-dimensional feature vector. It is neccessary to prepare the input for the next fully-connected (dense) layers

20. Dense layer, 100 units

21. BatchNormalization layer

22. Dense layer, 100 units

23. BatchNormalization layer

24. Dense layer, 16 units

In all of the hidden layers, the ReLU activation function was used. The output layer is combined with the softmax activation function, to obtain the classes probabilities (likelinesses). It was also found experimentally that addition of the BatchNormalization layers into the model significantly increases the model learning rate. That is, the NN without BatchNormalization reaches validation accuracy of ~0.3 after 10 training epochs, while NN with BatchNormalization layers included may reach validation accuracy of 0.5 and more during the same training period.

###Second approach

First model architecture described above gives accuracy of less than 0.7, which does not allow us to use it as a robust classifier. We assume that the accuracy may be increased if we take into account text content of the documents, instead of considering them as a whole images. Hence, text detection problem arises. To locate words of text on the document pages, the OpenCV EAST text detection NN was used. The EAST model is located in the file frozen_east_text_detection.pb.

With the positions of the words on the page, two further solutions were considered. At first, we could apply the text recognition model, such as Tesseract, to get the words from the image. Then the text analytics model should be built. Word recognition method has been tested on the samples from the initial dataset. Despite the fair recognition quality, the dataset with the texts from the recognized documents was not built due to the lack of computational resources. The notebook with the document text recognition could be found in the file text_recognition.ipynb.

According to the second solution, we count word distribution on the page in the following algorithm:

1. Find mean_width and mean_height of the word boxes.

2. If mean_width <= mean_height, then the document page is of album orientation.

3. Split the document page into zones corresponding to the lines of text on the page. The splitting is performed along the page height, if the document is of portrait orientation, and along the page width if it is of album orientation. Hence, we have the document consisting of zones of size 15 times width (portrait) or height (album). Here 15 is the fontsize, picked experimentally.

4. Count the number of word boxes in each zone. The word box is assigned to the zone if its top left corner lies within the zone boundaries.

5. Return a vector of size 992 // 15 + 1, which stores word counts for each of the zones, and a flag whether document page is of album orientation. The number 992 represents the document height, which is a multiple of 32, as required by the EAST model.

This vector could then be used as an input for the NN with fully-connected layers, to predict the document class.

###Conclusion

The second approach was not implemented, due to the lack of computational resources to extract the word distributions using the EAST NN, and only extraction code was developed. This code could be found in the file paragraph_statistics_nn.py. We also suggest that to maximize the model accuracy, both approaches should be combined. That is, there exist document types with significantly different word distributions, such as scientific report, memo and file folder. Such types can be gathered into families, for instance, with high and low word density. Hence, we may use the second approach to identify the family of the document, and then apply first approach, to identify document class inside the family. To do this, separate image recognition NN should be trained for each family.
