import tensorflow as tf


path = '/home'

physical_devices = tf.config.experimental.list_physical_devices('GPU')
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

def align_to_32(number):
    return 32 * (number // 32)

batch_size = 32 # 32
img_height = align_to_32(700) # 1000
img_width = align_to_32(int(700 * 775 / 1000)) # 775
num_classes = 16 # 16

datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    rescale=1./255
    # brightness_range=(-0.05, 0.05),
    # zoom_range=0.2
)
train_images = datagen.flow_from_directory(
    f'{path}/dataset/train',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='grayscale'
)
val_images = datagen.flow_from_directory(
    f'{path}/dataset/validation',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='grayscale'
)
test_images = datagen.flow_from_directory(
    f'{path}/dataset/test',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='grayscale'
)

path_to_model = input('Input path to your model: ')
model = tf.keras.models.load_model(path_to_model)

epochs = 20
steps_per_epoch = 9600 / batch_size
validation_steps = 3200 / batch_size
history = model.fit(
    x=train_images,
    steps_per_epoch=steps_per_epoch,
    validation_steps=validation_steps,
    validation_data=val_images,
    epochs=epochs
)

steps = 3200 / batch_size
loss, metrics = model.evaluate(x=test_images, steps=steps)
print(f'Test loss: {loss}\nTest accuracy: {metrics}')

ans = input('Save model? [Y/n]')
if ans.lower() == 'y':
    model.save(path_to_model)
    print(f'Model was saved to {path_to_model}')
else:
    print('Exit without save')
