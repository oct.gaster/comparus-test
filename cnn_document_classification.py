# -*- coding: utf-8 -*-
"""cnn_document_classification.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1RDGy6nBsz8tYD4sVcym2Fh9_BjXLQ6sm
"""

import os
import shutil
import json
import PIL
import pathlib
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential


path = '/home'

def fill_dataset_with_files(folder_name, filenames, class_names):
    for class_id in filenames:
        class_name = class_names[class_id]
        os.mkdir(f'{path}/dataset/{folder_name}/{class_name}')
        for filename in filenames[class_id]:
            src = f'{path}/interview/{filename}'
            dst = f'{path}/dataset/{folder_name}/{class_name}/{filename}'
            shutil.copyfile(src, dst)

physical_devices = tf.config.experimental.list_physical_devices('GPU')
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

with open(path + '/train_1000.json', 'r') as f:
    input_data = json.load(f)
with open(path + '/labels_desription.txt', 'r') as f:
    class_names = dict()
    for line in f:
        class_id, class_name = line.split(' ')
        class_names[class_id] = class_name[:-1]

# train_data = dict()
# val_data = dict()
# test_data = dict()
# for class_id in input_data:
#     train_data[class_id], val_data[class_id] = train_test_split(input_data[class_id], test_size=0.4)
#     val_data[class_id], test_data[class_id] = train_test_split(val_data[class_id], test_size=0.5)

# fill_dataset_with_files('train', train_data, class_names)
# fill_dataset_with_files('validation', val_data, class_names)
# fill_dataset_with_files('test', test_data, class_names)

def align_to_32(number):
    return 32 * (number // 32)

batch_size = 32 # 32
img_height = align_to_32(700) # 1000
img_width = align_to_32(int(700 * 775 / 1000)) # 775
num_classes = 16 # 16

datagen = keras.preprocessing.image.ImageDataGenerator(
    rescale=1./255
    # brightness_range=(-0.05, 0.05),
    # zoom_range=0.2
)
train_images = datagen.flow_from_directory(
    f'{path}/dataset/train',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='grayscale'
)
val_images = datagen.flow_from_directory(
    f'{path}/dataset/validation',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='grayscale'
)
test_images = datagen.flow_from_directory(
    f'{path}/dataset/test',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='grayscale'
)

#image, label = train_images.next()
#plt.figure(figsize=(20, 10))
#plt.imshow(image[0][:, :, 0], cmap='gray')
#print(type(label))

data_augmentation = tf.keras.Sequential([
    # layers.experimental.preprocessing.RandomFlip(mode='horizontal', input_shape=(img_height, img_width, 1)),
    # layers.experimental.preprocessing.RandomRotation(0 * np.pi / 180),
    layers.experimental.preprocessing.RandomZoom(0.2, input_shape=(img_height, img_width, 1)),
    layers.experimental.preprocessing.RandomContrast(0.1)
])

#plt.figure(figsize=(20, 20))
#images, _ = train_images.next()
#augmented_images = data_augmentation(images)
#for i in range(9):
#    ax = plt.subplot(3, 3, i + 1)
#    plt.imshow(augmented_images[i][:, :, 0], cmap='gray')
#    plt.axis('off')

input_retention = 0.9
conv_retention = 0.75
dense_retention = 0.5
model_augment_drop = Sequential([
    data_augmentation,
    # layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 1)),
    # layers.Dropout(1 - input_retention),
    layers.BatchNormalization(),
    layers.Conv2D(filters=8, kernel_size=5, padding='same', activation='relu'),
    layers.Conv2D(filters=8, kernel_size=3, padding='same', activation='relu'),
    # layers.Conv2D(filters=32, kernel_size=3, padding='same', activation='relu'),
    # layers.BatchNormalization(),
    layers.MaxPooling2D(pool_size=2, strides=3),
    layers.BatchNormalization(),
    # layers.Dropout(1 - conv_retention),
    # layers.Conv2D(filters=16, kernel_size=5, padding='same', activation='relu'),
    layers.Conv2D(filters=16, kernel_size=3, padding='same', activation='relu'),
    # layers.BatchNormalization(),
    layers.MaxPooling2D(strides=3),
    layers.BatchNormalization(),
    # layers.Dropout(1 - conv_retention),
    layers.Conv2D(filters=32, kernel_size=3, padding='same', activation='relu'),
    # layers.BatchNormalization(),
    layers.MaxPooling2D(strides=3),
    layers.BatchNormalization(),
    # layers.Dropout(1 - conv_retention),
    layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu'),
    #layers.BatchNormalization(),
    layers.MaxPooling2D(strides=3),
    layers.BatchNormalization(),
    # layers.Dropout(1 - conv_retention),
    layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu'),
    #layers.BatchNormalization(),
    layers.MaxPooling2D(strides=3),
    layers.BatchNormalization(),
    # layers.Conv2D(filters=128, kernel_size=3, padding='same', activation='relu'),
    # #layers.BatchNormalization(),
    # layers.MaxPooling2D(strides=3),
    # layers.BatchNormalization(),
    # layers.Dropout(1 - conv_retention),
    # layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu'),
    # # layers.BatchNormalization(),
    # layers.MaxPooling2D(strides=3),
    # layers.Dropout(1 - conv_retention),
    layers.Flatten(),
    layers.Dense(100, activation='relu'),
    # layers.Dropout(1 - dense_retention),
    layers.BatchNormalization(),
    # layers.Dense(1024, activation='relu'),
    # layers.Dropout(1 - dense_retention),
    layers.Dense(100, activation='relu'),
    # layers.Dropout(1 - dense_retention),
    layers.BatchNormalization(),
    layers.Dense(num_classes, activation='softmax')
])

model_augment_drop.compile(optimizer='adam',
                           loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
                           metrics=['accuracy',])

model_augment_drop.summary()

epochs = 20
steps_per_epoch = 9600 / batch_size
validation_steps = 3200 / batch_size
history = model_augment_drop.fit(
    x=train_images,
    steps_per_epoch=steps_per_epoch,
    validation_steps=validation_steps,
    validation_data=val_images,
    epochs=epochs
)

steps = 3200 / batch_size
loss, metrics = model_augment_drop.evaluate(x=test_images, steps=steps)
print(f'Test loss: {loss}\nTest accuracy: {metrics}')

model_augment_drop.save(f'{path}/model')

# acc = history.history['accuracy']
# val_acc = history.history['val_accuracy']
# loss = history.history['loss']
# val_loss = history.history['val_loss']
# epochs_range = range(epochs)
# plt.figure(figsize=(16, 8))
# plt.subplot(1, 2, 1)
# plt.plot(epochs_range, acc, label='Training Accuracy')
# plt.plot(epochs_range, val_acc, label='Validation Accuracy')
# plt.legend(loc='lower right')
# plt.title('Training and Validation Accuracy')
# plt.subplot(1, 2, 2)
# plt.plot(epochs_range, loss, label='Training Loss')
# plt.plot(epochs_range, val_loss, label='Validation Loss')
# plt.legend(loc='upper right')
# plt.title('Training and Validation Loss')
