#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import shutil
import json
import PIL
import pathlib
import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
from imutils.object_detection import non_max_suppression


path = '/home'


# In[2]:


zone_size = 15
img_height = 992
img_width = 768
batch_size = 16
num_classes = 16


def decode_predictions(scores, geometry, min_confidence):
    num_rows, num_cols = scores.shape[2:4]
    rects = []
    confidences = []
    for y in range(num_rows):
        scores_data = scores[0, 0, y]
        x_data0 = geometry[0, 0, y]
        x_data1 = geometry[0, 1, y]
        x_data2 = geometry[0, 2, y]
        x_data3 = geometry[0, 3, y]
        angles_data = geometry[0, 4, y]
        for x in range(num_cols):
            if scores_data[x] < min_confidence:
                continue
            offset_x, offset_y = x * 4, y * 4
            angle = angles_data[x]
            cos = np.cos(angle)
            sin = np.sin(angle)
            h = x_data0[x] + x_data2[x]
            w = x_data1[x] + x_data3[x]
            end_x = int(offset_x + cos * x_data1[x] + sin * x_data2[x])
            end_y = int(offset_y - sin * x_data1[x] + cos * x_data2[x])
            start_x = int(end_x - w)
            start_y = int(end_y - h)
            rects.append((start_x, start_y, end_x, end_y))
            confidences.append(scores_data[x])
    return rects, confidences

# choose two particular layers of the EAST NN as output
layer_names = ['feature_fusion/Conv_7/Sigmoid', 'feature_fusion/concat_3']
# read saved EAST model
net = cv2.dnn.readNet(f'{path}/east_text_detection/frozen_east_text_detection.pb')


def get_word_statistics(image, zone_size, img_width, img_height):
    new_w = img_width
    new_h = img_height
    # load image to the Binary Large OBject instance, to feed it into the EAST NN
    blob = cv2.dnn.blobFromImage(
        image,
        1.0,
        (new_w, new_h),
        (103, ) * 3,
        swapRB=True,
        crop=False
    )
    net.setInput(blob)
    scores, geometry = net.forward(layer_names)
    rects, confidences = decode_predictions(scores, geometry, min_confidence=0.5)
    # There are several boxes found in the image which correspond to the same word.
    # The code below takes only one of this boxes, for which the word recognition confidence
    # is the biggest
    boxes = non_max_suppression(np.array(rects), probs=confidences)
    mean_boxwidth = np.mean([end_x - start_x for start_x, _, end_x, _ in boxes])
    mean_boxheight = np.mean([end_y - start_y for _, start_y, _, end_y in boxes])
    is_album = mean_boxwidth <= mean_boxheight
    page_height = new_w if is_album else new_h
    word_statistics = []
    if is_album:
        for z in range(zone_size, page_height, zone_size):
            k = len([start_x for start_x, _, _, _ in boxes if z - zone_size <= start_x < z])
            word_statistics.append(k)
    else:
        for z in range(zone_size, page_height, zone_size):
            k = len([start_y for _, start_y, _, _ in boxes if z - zone_size <= start_y < z])
            word_statistics.append(k)
    return [int(is_album),] + word_statistics


# In[3]:


datagen = keras.preprocessing.image.ImageDataGenerator()
train_images = datagen.flow_from_directory(
    f'{path}/dataset/train',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='rgb'
)
val_images = datagen.flow_from_directory(
    f'{path}/dataset/validation',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='rgb'
)
test_images = datagen.flow_from_directory(
    f'{path}/dataset/test',
    class_mode='categorical',
    batch_size=batch_size,
    target_size=(img_height, img_width),
    color_mode='rgb'
)


# In[ ]:


arr = []
column_count = max(img_height, img_width) // zone_size
for images_batch, labels_batch in train_images:
    for i in range(batch_size):
        # extract word distribution for the current image in the training dataset
        document_properties = [labels_batch[i],] + get_word_statistics(
        images_batch[i],
        zone_size=zone_size,
        img_width=img_width,
        img_height=img_height
    )
    arr.append(document_properties)
# dataframe with columns corresponding to the zones of document
word_statistics_df = pd.DataFrame(arr, columns=['document_class', 'is_album']
                                  + [str(i) for i in range(column_count)])
word_statistics_df.to_csv(f'{path}/word_statistics_train', index=False)
