# COMPARUS TEST

This is the deep learning model for the document classification. Files with the model results are as follows:
  * cnn_document_classification.py -- the final model implementation. This script trains the model, evaluates its accuracy on the test dataset and automatically saves the model into the folder 'model'
  * further_training.py -- loads already trained model and continues its training during 20 epochs
  * final_model folder -- the trained model
  * paragraph_statistics_nn.py -- counts word distributions of the documents inside the training dataset and saves the result to the CSV file 'word_statistics_train'
  * text_recognition.ipynb -- notebook with the text recognition test
  * frozen_east_text_detection.pb -- saved OpenCV EAST text detection model
  * comparus_report.md -- descriptions of the implemented solution, other considered solutions and research results
  * links.md -- contains link to the structured dataset
